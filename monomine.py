#
# monomine - A simple ticket manager for Redmine
#
# Copyright (C) Fujimoto Seiji, 2019

import time
import json
import os
import ssl
import datetime
import configparser
import urllib.request
import urllib.parse
import flask

#
# GLOBALS

CACHE = {}
CONFIG = None
MTIME = 0
CONFPATH = 'app.conf'

#
# Utils

def mtime(path):
    return os.stat(path).st_mtime

def config_load(path):
    config = configparser.ConfigParser()
    with open(path, 'r') as fp:
        config.readfp(fp)
    return dict(config)

def dt_iso8601(dtstr):
    dt = datetime.datetime.strptime(dtstr, '%Y-%m-%dT%H:%M:%SZ')
    return dt + datetime.timedelta(hours=9)

def dt_reldays(dt):
    if isinstance(dt, str):
        dt = dt_iso8601(dt)

    days = (datetime.date.today() - dt.date()).days
    secs = (datetime.datetime.now() - dt).total_seconds()
    if secs < 3600:
        res = '%s分前' % int(secs / 60)
    elif days == 0:
        res = '%s時間前' % int(secs / 3600)
    elif dt.year == datetime.date.today().year:
        res = dt.strftime("%-m/%-d %H:%M")
    else:
        res = dt.strftime("%Y/%-m/%-d<br>%H:%M")
    return res

#
# Redmine functions

def redmine_api(target, params):
    url = '{baseurl}/{target}?{params}'.format(
        baseurl=CONFIG['core']['url'],
        target=target,
        params=urllib.parse.urlencode(params))

    if not url.startswith('https://'):
        raise RuntimeError('HTTP is INSECURE. Use HTTPS for core.url')

    if url in CACHE:
        return CACHE[url]

    context = ssl.create_default_context()

    # Proceed to request if no cache is available.
    req = urllib.request.Request(url, headers={
        'X-Redmine-API-Key': CONFIG['core']['apikey'],
    })
    with urllib.request.urlopen(req, context=context) as fp:
        CACHE[url] = json.loads(fp.read().decode())

    return CACHE[url]

def redmine_match(ticket, query):
    text = '{id} {subject} {author} {status[name]} {project[name]}'.format(**ticket)
    text = text.lower()

    for q in query.lower().split(' '):
        q = q.strip()
        if q.startswith('-'):
            if q[1:] in text:
                return False
        else:
            if q not in text:
                return False

    return True

#
# Main

app = flask.Flask(__name__)

app.config['TEMPLATES_AUTO_RELOAD'] = 1

app.add_template_filter(dt_reldays)

@app.route('/')
def pg_index():
    global CONFIG, MTIME

    q = flask.request.args.get('q', '')

    if flask.request.headers.get('Cache-Control') in ('max-age=0', 'no-cache'):
        CACHE.clear()

    if mtime(CONFPATH) > MTIME:
        CONFIG = config_load(CONFPATH)
        MTIME = mtime(CONFPATH)
        CACHE.clear()

    pos = 0
    tickets = []
    while True:
        resp = redmine_api('issues.json', {
            'offset': pos,
            'limit': 100,
            'sort': 'updated_on:desc'
        })
        for tk in resp['issues']:
            if 'assigned_to' in tk:
                tk['author'] = tk['assigned_to']

            if redmine_match(tk, q):
                tickets.append(tk)
        pos += len(resp['issues'])
        if resp['total_count'] <= pos:
            break
        if len(tickets) >= 100:
            break

    return flask.render_template('index.html',
                                 tickets=tickets,
                                 CONFIG=CONFIG)

if __name__ == '__main__':
    app.run()
