monomine
========

monomine is a simple (but very fast) ticket viewer for [Redmine](https://www.redmine.org/).

It does not do lots things, but it _does_ enable you to navigate and
search issue tickets very efficiently.

QUICKSTART
----------

Install flask and gunicorn.

    $ sudo apt install python3-flask gunicorn3

Prepare app.conf.

    # Copy the configuration template to `app.conf`
    $ cp app.conf.tmpl app.conf
    $ chmod 600 app.conf

    # Set your Redmine server and add your API key.
    $ vim app.conf

Run the monomine application.

    $ gunicorn3 monomine:app

Access to localhost:8000 and see if it works fine.

    $ firefox http://localhost:8000/


HOW IT WORKS
------------

This application is essentially a JSON fetcher. It retrieves the list of
issues in JSON format and display the result as HTML.

            +----------+     +---------+
    YOU --> | monomine | ==> | Redmine |
            +----------+     +---------+

The major source of efficiency is that it _caches_ API responses very
transparently. So monomine often can show tickets much faster than
a direct access to Redmine.

The security model is very simple too. As long as:

 1) you are running monomine on localhost, and
 2) you are using HTTPS to connect to Redmine

... it should be secure.
